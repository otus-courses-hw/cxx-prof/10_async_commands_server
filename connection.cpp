#include <boost/asio/io_context.hpp>
#include <boost/asio/placeholders.hpp>
#include <boost/bind/bind.hpp>
#include <iostream>
#include "session.hpp"
#include "connection.hpp"

connection::connection(boost::asio::io_context &io_context, session::pointer session) :
    socket_(io_context),
    session_(session)
{
}

auto connection::create(boost::asio::io_context &io_context, session::pointer session) -> pointer
{
    return pointer(new connection(io_context, session));
}

tcp::socket &connection::socket()
{
    return socket_;
}

void connection::start()
{
    socket_.async_read_some(boost::asio::buffer(buff_, buff_.size()), boost::bind(
                &connection::handle_read,
                shared_from_this(),
                boost::asio::placeholders::error));
}

void connection::handle_read(const boost::system::error_code &error)
{
    if (error.value() == boost::system::errc::success)
        session_->receive(buff_.data());
    else
        std::cerr << "error: " << error.message() << std::endl;
}
