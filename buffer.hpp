#include <chrono>
#include <atomic>

using TimePoint = std::chrono::time_point<std::chrono::system_clock>;

template <typename Container>
class BufferBase
{
    public:
        BufferBase() = delete;
        BufferBase(unsigned short);
        ~BufferBase() = default;

        unsigned short threshold() const;
        void set_threshold(const unsigned short);
        TimePoint time() const;
        void time_refresh();
        unsigned short used() const;

        virtual void clear() = 0;
        virtual void add(typename Container::value_type) = 0;
        virtual typename Container::const_iterator cbegin() const = 0;
        virtual typename Container::const_iterator cend() const = 0;

    protected:
        Container m_buff;
        std::atomic<unsigned short> m_used;

    private:
        std::atomic<unsigned short> m_threshold; //threshold may be mutable when used in async mode
        TimePoint m_time;
};

template <typename Vector>
class Buffer : public BufferBase<Vector>
{
    public:
        Buffer() = delete;
        Buffer(unsigned short);
        ~Buffer() = default;

        void add(typename Vector::value_type) override;
        void clear() override;
        typename Vector::const_iterator cbegin() const override;
        typename Vector::const_iterator cend() const override;
};

template <typename Container>
BufferBase<Container>::BufferBase(unsigned short size) :
    m_used{0},
    m_threshold{size}
{
}

template <typename Container>
unsigned short BufferBase<Container>::threshold() const
{
    return m_threshold;
}

template <typename Container>
void BufferBase<Container>::set_threshold(const unsigned short size)
{
    m_threshold = size;
}

template <typename Container>
TimePoint BufferBase<Container>::time() const
{
    return m_time;
}

template <typename Container>
void BufferBase<Container>::time_refresh()
{
    m_time = std::chrono::system_clock::now();
}

template <typename Container>
unsigned short BufferBase<Container>::used() const
{
    return m_used;
}

template <typename Vector>
Buffer<Vector>::Buffer(unsigned short size) :
    BufferBase<Vector>::BufferBase(size)
{
    BufferBase<Vector>::m_buff.reserve(size);
}

template <typename Vector>
void Buffer<Vector>::add(typename Vector::value_type val)
{
    if (BufferBase<Vector>::m_buff.empty())
        BufferBase<Vector>::time_refresh();

    BufferBase<Vector>::m_buff.push_back(val);
    BufferBase<Vector>::m_used++;
}

template <typename Vector>
void Buffer<Vector>::clear()
{
    BufferBase<Vector>::m_used = 0;
    BufferBase<Vector>::m_buff.clear();
    BufferBase<Vector>::m_buff.reserve(BufferBase<Vector>::threshold());
}

template <typename Vector>
typename Vector::const_iterator Buffer<Vector>::cbegin() const
{
    return BufferBase<Vector>::m_buff.cbegin();
}

template <typename Vector>
typename Vector::const_iterator Buffer<Vector>::cend() const
{
    return BufferBase<Vector>::m_buff.cend();
}

