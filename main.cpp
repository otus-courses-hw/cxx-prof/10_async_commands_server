#include "app.hpp"
#include "reader.hpp"
#include "writer.hpp"
#include "server.hpp"
#include <iostream>
#include <memory>

int main(int argc, char* argv[])
{
  try
  {
    if (argc != 2)
    {
      std::cerr << "Usage: async_tcp_echo_server <port>\n";
      return 1;
    }

    App::pointer app(new App(3)); 

    using iter_type = typename App::BuffIter;
    std::shared_ptr<IWriter<iter_type>> writer(new MultiWriter<iter_type>(
                std::make_unique<CliWriter<iter_type>>(1),
                std::make_unique<FileWriter<iter_type>>(2)
                ));

    app->set_writer(writer);

    std::shared_ptr<IReader> reader(new StreamReader(app));
    app->set_reader(reader);

    boost::asio::io_context io_context;
    server server(io_context, std::atoi(argv[1]));
    server.deploy(app);

    io_context.run();
  }
  catch (const std::exception& ex)
  {
    std::cerr << "Exception: " << ex.what() << "\n";
  }

  return 0;
}
