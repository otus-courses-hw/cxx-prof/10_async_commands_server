#include "reader.hpp"
#include "app.hpp"

#include <istream>
#include <limits>
#include <iostream>
#include <sstream>

BaseReader::BaseReader(AppPtr app)
{
    m_app = app;

    using namespace std::placeholders;

    ctrl_map['{'] = std::bind(&BaseReader::add_block, this, _1);
    ctrl_map['}'] = std::bind(&BaseReader::del_block, this, _1);
    ctrl_map[EOF] = std::bind(&BaseReader::end, this, _1);
}

void BaseReader::add_cmd(const std::string& cmd) const
{
    m_app->add_cmd(cmd);
}

std::istream &BaseReader::add_block(std::istream& stream) const
{
    m_app->add_block();
    return ignore(stream);
}

std::istream &BaseReader::del_block(std::istream& stream) const
{
    m_app->del_block();
    return ignore(stream);
}

std::istream &BaseReader::end(std::istream& stream) const
{
    m_app->end();
    ignore(stream).clear();
    clearerr(stdin);
    return stream;
}

bool BaseReader::is_ctrl_cmd(const char c) const
{
    return ctrl_map.find(c) != ctrl_map.end();
}

void BaseReader::send_ctrl_cmd(const char c, std::istream &is)
{
    ctrl_map.at(c)(is);
}

std::istream &BaseReader::ignore(std::istream &stream) const
{
    stream.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    return stream;
}

CliReader::CliReader(AppPtr app) :
    BaseReader(app)
{
}

void CliReader::read(std::istream &is)
{
    while(is)
    {
        char c = is.peek();
        if (BaseReader::is_ctrl_cmd(c))
        {
            BaseReader::send_ctrl_cmd(c, is);
        }
        else
        {
            std::string token;
            std::getline(is, token);
            BaseReader::add_cmd(token);
        }

        std::this_thread::sleep_for(std::chrono::seconds(1));
    }
}

StreamReader::StreamReader(AppPtr app) :
    BaseReader(app)
{
}

void StreamReader::read(std::istream &is)
{
    while(is)
    {
        char c = is.peek();
        if (BaseReader::is_ctrl_cmd(c) && c != EOF) // ignore EOF because of possible multiple client
        {
            BaseReader::send_ctrl_cmd(c, is);
        }
        else if (c != EOF) // otherwise get the empty string "" as cmd
        {
            std::string token;
            std::getline(is, token);
            BaseReader::add_cmd(token);
        }
    }
}
