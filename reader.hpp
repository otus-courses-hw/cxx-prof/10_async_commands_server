#ifndef READER_HPP
#define READER_HPP

#include <iostream>
#include <istream>
#include <memory>
#include <unordered_map>
#include <string>
#include <functional>

struct IReader
{
    virtual void add_cmd(const std::string&) const  = 0;
    virtual std::istream &add_block(std::istream&) const = 0;
    virtual std::istream &del_block(std::istream&) const = 0;
    virtual std::istream &end(std::istream&) const = 0;

    virtual void read(std::istream&) = 0;

};

class App;
using AppPtr = std::shared_ptr<App>;

class BaseReader : public IReader
{
    public:
        BaseReader(AppPtr);
        void add_cmd(const std::string&) const override;
        std::istream &add_block(std::istream&) const override;
        std::istream &del_block(std::istream&) const override;
        std::istream &end(std::istream&) const override;


    protected:
        std::unordered_map<char, std::function<void(std::istream&)>> ctrl_map;

        bool is_ctrl_cmd(const char) const;

        void send_ctrl_cmd(const char, std::istream&);

    private:
        AppPtr m_app;

        std::istream &ignore(std::istream&) const;

};

class CliReader : public BaseReader
{
    public:
        CliReader(AppPtr);

        void read(std::istream&) override;
};

class StreamReader : public BaseReader
{
    public:
        StreamReader(AppPtr);

        void read(std::istream&) override;
};

#endif /* READER_HPP */
