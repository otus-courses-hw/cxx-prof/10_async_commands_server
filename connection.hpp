#include <bits/c++config.h>
#include <boost/asio.hpp>
#include <boost/asio/io_context.hpp>
#include <boost/system/error_code.hpp>
#include "session.hpp"

using tcp = boost::asio::ip::tcp;

class connection : public std::enable_shared_from_this<connection>
{
    public:
        using pointer = std::shared_ptr<connection>;

        static pointer create(boost::asio::io_context&, session::pointer);

        tcp::socket &socket();

        void start();

    private:
        static const std::size_t buf_size_ = 1024;
        tcp::socket socket_;
        std::array<char, buf_size_> buff_;
        session::pointer session_;

        connection(boost::asio::io_context&, session::pointer);

        void handle_read(const boost::system::error_code&);

};
