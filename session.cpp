#include "session.hpp"
#include <iostream>
#include <future>

session::session(receiver task) :
    task_(task)
{
}

std::string session::data()
{
    return stream_.str();
}

void session::receive(const char* data)
{
    stream_ << data;
    auto f = std::async(std::launch::async, task_, shared_from_this());
    f.get();
}
