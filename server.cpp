#include "server.hpp"
#include <boost/asio/placeholders.hpp>
#include <boost/bind.hpp>
#include <boost/system/error_code.hpp>
#include <iostream>

server::server(boost::asio::io_context &io_context, short port) :
    io_context_(io_context),
    acceptor_(io_context, tcp::endpoint(tcp::v4(), port)),
    app_(nullptr),
    task_(nullptr)
{
}

void server::start_accept()
{
    session::pointer ses(new session(task_));
    connection::pointer new_connection = connection::create(io_context_, ses);

    acceptor_.async_accept(new_connection->socket(), boost::bind(
                &server::handle_accept,
                this,
                new_connection,
                boost::asio::placeholders::error));
}

void server::handle_accept(connection::pointer connection, const boost::system::error_code &error)
{
    if (error.value() == boost::system::errc::success)
        connection->start();
    else
        std::cerr << "error: " << error.message() << std::endl;

    start_accept();
}

void server::deploy(App::pointer app)
{
    app_ = app;
    task_ = std::bind(&App::receive, app_, std::placeholders::_1);
    start_accept();
}
