#ifndef COMMAND_HANDLER_HPP
#define COMMAND_HANDLER_HPP

class App;

struct ICmdUserHandler
{
    virtual void add_cmd(App *) = 0;
    virtual void add_block(App *) = 0;
    virtual void del_block(App *) = 0;
    virtual void end(App *) = 0;
};

struct ProcessCmdDynamic : public ICmdUserHandler
{
    void add_cmd(App *) override;
    void add_block(App *) override;
    void del_block(App *) override;
    void end(App *) override;
};

struct ProcessCmdStatic : public ICmdUserHandler
{
    void add_cmd(App *) override;
    void add_block(App *) override;
    void del_block(App *) override;
    void end(App *) override;
};

#endif /*COMMAND_HANDLER_HPP*/
