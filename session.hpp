#ifndef SESSION_HPP
#define SESSION_HPP

#include <sstream>
#include <string>
#include <memory>
#include <functional>

class session : public std::enable_shared_from_this<session>
{
    public:
        using pointer = std::shared_ptr<session>;
        using receiver = std::function<void(pointer)>;

        session(receiver);
        std::string data();
        void receive(const char*);

    private:
        receiver task_;
        std::stringstream stream_;
};

#endif /*SESSION_HPP*/
