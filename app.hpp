#ifndef APP_HPP
#define APP_HPP

#include <stdexcept>
#include <string>
#include <vector>
#include <memory>
#include <mutex>
#include <atomic>

#include "writer.hpp"
#include "reader.hpp"
#include "command_handler.hpp"
#include "buffer.hpp"
#include "command.hpp"
#include "session.hpp"

class App
{
    public:
        App() = delete;
        App(unsigned short);

        friend struct ProcessCmdStatic;
        friend struct ProcessCmdDynamic;

        using pointer = std::shared_ptr<App>;

        template <typename T>
        using Container = std::vector<T>;

        using BuffPtr = std::shared_ptr<BufferBase<Container<Command>>>;

        using BuffIter = Container<Command>::const_iterator;
        
        using IWriterPtr = std::shared_ptr<IWriter<BuffIter>>;

        using IReaderPtr = std::shared_ptr<IReader>;
        
        using ICmdUserHandlerPtr = std::unique_ptr<ICmdUserHandler>;

        void set_writer(IWriterPtr);
        void set_reader(IReaderPtr);
        void add_cmd(std::string);
        void add_block();
        void del_block();
        void end() noexcept(false);
        void process_cmd();

        void receive(session::pointer);

    private:
        std::atomic<unsigned short> inner_block_cmd;
        ICmdUserHandlerPtr m_handler;
        BuffPtr m_buff;
        IWriterPtr m_writer;
        IReaderPtr m_reader;
        std::mutex m_mutex;
};

#endif /*APP_HPP*/
