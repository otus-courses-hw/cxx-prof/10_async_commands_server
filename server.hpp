#include <boost/asio.hpp>
#include <boost/asio/io_context.hpp>
#include <boost/system/error_code.hpp>
#include "connection.hpp"
#include "app.hpp"
#include "session.hpp"

using tcp = boost::asio::ip::tcp;

class server
{
    public:
        server(boost::asio::io_context&, short);

        void deploy(App::pointer);

    private:
        boost::asio::io_context &io_context_;
        tcp::acceptor acceptor_;
        App::pointer app_;
        session::receiver task_;

        void start_accept();

        void handle_accept(connection::pointer, const boost::system::error_code&);
};
