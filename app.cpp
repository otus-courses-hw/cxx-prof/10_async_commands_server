#include <mutex>
#include <stdexcept>
#include <utility>

#include "app.hpp"

App::App(unsigned short size) :
    inner_block_cmd{0},
    m_handler(new ProcessCmdStatic()),
    m_buff(new Buffer<Container<Command>>(size)),
    m_writer(nullptr),
    m_reader(nullptr)
{
}

void App::set_writer(IWriterPtr writer)
{
    m_writer = writer;
}

void App::set_reader(IReaderPtr reader)
{
    m_reader = reader;
}

void App::add_cmd(std::string cmd)
{
    std::lock_guard lk(m_mutex);

    m_buff->add(cmd);
    m_handler->add_cmd(this);
}

void App::add_block()
{
    m_handler->add_block(this);
}

void App::del_block()
{
    m_handler->del_block(this);
}

void App::process_cmd()
{
    auto when_block_started = m_buff->time();
    m_writer->write(m_buff->cbegin(), m_buff->cend(), when_block_started);
    m_buff->clear();
}

void App::end()
{
    m_handler->end(this);
    m_writer->stop();
    exit(1);
}

void App::receive(session::pointer session)
{
    std::istringstream iss(session->data());
    m_reader->read(iss);
}
