cmake_minimum_required(VERSION 3.11)
project(hw10_asio_server)

set(GCC_LIKE_CXX $<$<COMPILE_LANG_AND_ID:CXX,ARMClang,AppleClang,Clang,GNU,LCC>:-Wall -Wextra -pedantic -Werror>)
set(MSVC_CXX $<$<COMPILE_LANG_AND_ID:CXX,MSVC>:-W4>)
set(THREADS_PREFER_PTHREAD_FLAG ON)
set(Boost_USE_STATIC_LIB OFF)
set(Boost_USE_MULTITHREADED ON)

option(WITH_TEST OFF)

find_package(Boost 1.71 REQUIRED
    COMPONENTS system filesystem unit_test_framework)
find_package(Threads REQUIRED)

add_executable(app
    main.cpp
    reader.cpp
    app.cpp
    command_handler.cpp
    connection.cpp
    session.cpp
    server.cpp)
target_include_directories(app PUBLIC ${Boost_INCLUDE_DIRS})
target_link_libraries(app
    PRIVATE Threads::Threads)
set_target_properties(app PROPERTIES
    CXX_STANDARD 17
    CXX_STANDARTD_REQUIRED ON)

if(WITH_TEST)
    add_executable(test_all
        test_all.cpp
        test_session.cpp
        session.cpp)
    set_target_properties(test_all PROPERTIES
        CXX_STANDARD 17
        CXX_STANDARTD_REQUIRED ON)
    target_link_libraries(test_all
        ${Boost_FILESYSTEM_LIBRARY}
        ${Boost_SYSTEM_LIBRARY}
        ${Boost_UNIT_TEST_FRAMEWORK_LIBRARY})
endif(WITH_TEST)
